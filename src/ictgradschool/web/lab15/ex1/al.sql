DROP TABLE IF EXISTS access_log;
CREATE TABLE IF NOT EXISTS access_log(
  id INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
  name VARCHAR(40),
  description VARCHAR(40),
  ts TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

INSERT INTO access_log(name,description,ts) VALUES ('Robin', 'Cool', NOW());
INSERT INTO access_log(name,description,ts) VALUES ('Paul', 'Awesome', '2018-01-01' );
INSERT INTO access_log(name,description,ts) VALUES ('John', 'Fantastic', '2019-01-01');