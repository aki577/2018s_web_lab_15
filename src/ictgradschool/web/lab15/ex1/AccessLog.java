package ictgradschool.web.lab15.ex1;

import java.io.Serializable;
import java.sql.Timestamp;

public class AccessLog implements Serializable {
    private int id;
    private String name;
    private String description;
    private Timestamp timeStamp;

    public AccessLog(String name, String description, Timestamp timeStamp){
        this.name=name;
        this.description=description;
        this.timeStamp=timeStamp;
    }

    public AccessLog(int id, String name, String description, Timestamp timeStamp) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.timeStamp = timeStamp;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Timestamp getTimeStamp() {
        return timeStamp;
    }

}
