package ictgradschool.web.lab15.ex1;

import javax.servlet.ServletContext;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class AccessLogDAO {
    public List<AccessLog> allAccessLogs(ServletContext context) {
        Properties dbProps = new Properties();
        try (FileInputStream fIn = new FileInputStream(context.getRealPath("WEB-INF/mysql.properties"))) {
                Class.forName("org.mariadb.jdbc.Driver");
            dbProps.load(fIn);
        } catch (IOException| ClassNotFoundException e) {
            e.printStackTrace();
        }

        List<AccessLog> logs = new ArrayList<>();
        try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {
            try (Statement stmt = conn.createStatement()) {
                try (ResultSet rs = stmt.executeQuery("SELECT * FROM access_log")) {
                    while (rs.next()) {
                        logs.add(new AccessLog(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getTimestamp(4)));
                    }
                }
            }
        } catch (SQLException err) {
            err.printStackTrace();
        }
        System.out.println("logs size =" + logs.size());
        return logs;
    }

    public void addAccessLog(AccessLog a,ServletContext context) {
        Properties dbProps = new Properties();
        try (FileInputStream fIn = new FileInputStream(context.getRealPath("WEB-INF/mysql.properties"))) {
            Class.forName("org.mariadb.jdbc.Driver");
            dbProps.load(fIn);
        } catch (IOException| ClassNotFoundException e) {
            e.printStackTrace();
        }

        try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {
            try (Statement stmt = conn.createStatement()) {
                stmt.executeLargeUpdate("INSERT INTO access_log(name,description,ts) VALUES('" + a.getName() + "', '" + a.getDescription() + "', '" + a.getTimeStamp() + "')");
            }
        } catch (SQLException err) {
            err.printStackTrace();
        }
    }
}
