package ictgradschool.web.lab15.ex4;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.*;

public class JSONSavingServlet extends HttpServlet {
    private String transactionDir;

    /**
     * @param servletConfig a ServletConfig object containing information gathered when
     *                      the servlet was created, including information from web.xml
     */
    public void init(ServletConfig servletConfig) throws ServletException {
        super.init(servletConfig);
        this.transactionDir = servletConfig.getInitParameter("jsonDirectory");
        this.transactionDir = getServletContext().getRealPath(this.transactionDir);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        /* In this method, retrieve the submitted parameters and generate   *
         * a JSON document to represent the data. An example of creating    *
         * a JSON document using the json-simple library can be seen below. */

        /* If any parameter is missing, redirect the user to the form again *
         * by calling the doGet(request, response); method. Use those       *
         * parameters to populate the form, and indicate the missing values */
        int in = Integer.parseInt(request.getParameter("invoicenum"));
        String address = request.getParameter("address");
        String cardName = request.getParameter("cardName");
        String cardProvider = request.getParameter("cardProvider");
        String cardNumber = request.getParameter("cardNumber");
        if (request.getParameter("invoicenum") == null || address == null || cardName == null || cardProvider == null || cardNumber == null) {
            doGet(request, response);
        }

        JSONObject transaction = new JSONObject();

        // Adds a String value to the JSON
        transaction.put("key", "value");

        // Create and populate a JSON array
        JSONArray jsonArray = new JSONArray();
        jsonArray.add("e");
        jsonArray.add(1);
        jsonArray.add(false);
        jsonArray.add(null);

        // Add the array to our object
        transaction.put("array", jsonArray);

        JSONObject info = new JSONObject();
        info.put("billingAddress", address);

        JSONArray creditCard = new JSONArray();
        JSONObject cn = new JSONObject();
        JSONObject ct = new JSONObject();
        JSONObject cnum = new JSONObject();

        cn.put("cardName", cardName);
        ct.put("cardType", cardProvider);
        cnum.put("cardNo", cardNumber);
        creditCard.add(cn);
        creditCard.add(ct);
        creditCard.add(cnum);

        info.put("creditcard", creditCard);

        File jsonFile = new File(this.transactionDir, in + ".json");
        saveJSONObject(jsonFile, info);
        response.sendRedirect("JSONForm.jsp");
        /* Save the JSON object to an appropriate directory, using the    *
         * 'invoice number' parameter as the filename, with the extension *
         * '.json'. An example filename might be '15678.json'. The method *
         * saveJSONObject() has been provided to save the JSON            */
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // TODO: Display your Exercise 4 JSP file
        request.getRequestDispatcher("JSONForm.jsp").forward(request, response);
        if (request.getParameter("invoicenum") == null) {

        }
    }

    /**
     * Writes the given JSONObject (in JSON format) to the specified file path
     *
     * @param file
     * @param jsonRecord
     * @return true if file written successfully, false otherwise
     */
    private boolean saveJSONObject(File file, JSONObject jsonRecord) {
        if (file.exists()) {
            return false;
        }

        try (BufferedWriter writer = new BufferedWriter(new FileWriter(file))) {
            writer.write(JSONObject.toJSONString(jsonRecord));
        } catch (IOException e) {
            System.err.println(file.toString());
            System.err.println(jsonRecord.toJSONString());
            e.printStackTrace();
            return false;
        }

        return true;
    }
}
