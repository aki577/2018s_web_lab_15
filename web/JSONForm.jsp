<%--
  Created by IntelliJ IDEA.
  User: ww155
  Date: 18/01/2019
  Time: 10:48 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<p></p>
<form action="question4" method="post">
    <label for="invoicenumID">Invoice Number</label>
    <input type="text" id="invoicenumID" name="invoicenum">
    <br>
    <label for="addressID">Billing Address</label>
    <br>
    <textarea id="addressID"
              name="address"
              rows="3"
              cols="40">Enter your billing address here...</textarea>
    <br>
    <label for="cardNameID">Credit Card Name</label>
    <input type="text" id="cardNameID" name="cardName">
    <br>
    <label for="cardProviderID">Credit Card Provider</label>
    <input type="text" id="cardProviderID" name="cardProvider">
    <br>
    <label for="cardNumberID">Credit Card Number</label>
    <input type="text" id="cardNumberID" name="cardNumber"><br>
    <input type="submit" value="Submit!">
</form>
</body>
</html>
