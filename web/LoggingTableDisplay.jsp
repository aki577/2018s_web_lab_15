<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Exercise 01</title>
    <style type="text/css">
    table {
        border-collapse: collapse;
        width: 100%;
    }

    th {
        background-color: #00FFFF;
        color: white;
    }

    th, td {
        text-align: left;
        padding: 8px;
    }
</style>
</head>

<body>
<p>Generate a well formed table of AccessLogs here using JSTL/EL.</p>
<form action="question1/new" method="post">
    <label for="nameId">Name</label>
    <input type="text" id="nameId" name="name">
    <label for="desId">Description</label>
    <input type="text" id="desId" name="des">
    <input type="submit" value="Submit!">
</form>
<table>
    <thead>
    <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Description</th>
        <th>Time Stamp</th>
    </tr>
    </thead>
    <c:forEach items="${table}" var="log">
        <tr>
            <td>${log.id}</td>
            <td>${log.name}</td>
            <td>${log.description}</td>
            <td>${log.timeStamp}</td>
        </tr>
    </c:forEach>
    <tfoot>
    <tr>
        <td>ID</td>
        <td>Name</td>
        <td>Description</td>
        <td>Time Stamp</td>
    </tr>
    </tfoot>
</table>
</body>
</html>
